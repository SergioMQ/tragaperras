document.addEventListener('DOMContentLoaded', function () {
    // Variabales
    let palanca = document.querySelector('#palanca');
    let misNumeros = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    let ventana1 = document.querySelector('#ventana1');
    let ventana2 = document.querySelector('#ventana2');
    let ventana3 = document.querySelector('#ventana3');
    let creditoActual = document.querySelector('#credito_actual');
    let insertCoin = document.querySelector('#insert_coin');
    let misCreditos = 10;
    let mensajeVictoria = document.querySelector('#mensaje_victoria');
    let botonCerrarMensajeVictoria = document.querySelector('#cerrar_mensaje_vicotoria');
    let recuadroVictoria = document.querySelector('#recuadro_victoria');
    let mensajeDerrota = document.querySelector('#mensaje_derrota');
    let botonCerrarMensajeDerrota = document.querySelector('#cerrar_mensaje_derrota');
    let recuadroDerrota = document.querySelector('#recuadro_derrota');

    // Funciones
    /// Añadimos la clase de la palanca
    function animarPalanca (){
        palanca.classList.add('palanca_animada');
        eliminarClasePalanca();
        restarCreditos();
        tirada();
    }
    /// Eliminamos la clase de la palanca
    function eliminarClasePalanca () {
        setTimeout(eliminarClase, 1500);
    }
    function eliminarClase () {
        palanca.classList.remove('palanca_animada');
    }
    /// Sistema de monedas
    //// Añadimos creditos
    function sumarCreditos () {
        misCreditos += 1;
        creditoActual.textContent = misCreditos + ' créditos';
    }
    /// Restamos creditos cuando se active la palanca
    function restarCreditos () {
        misCreditos --;
        creditoActual.textContent = misCreditos + ' créditos';
        if(misCreditos <= -1) {
            mensajeDerrota.classList.add('is-active');
            creditoActual.textContent = '0 créditos';
            misCreditos = 0;
        }
    }
    /// Salen números al azahar
    function tirada () {
        for(let num of misNumeros) {
            ventana1.textContent = misNumeros[_.random(misNumeros.length - 1)];
        }
        for(let num of misNumeros) {
            ventana2.textContent = misNumeros[_.random(misNumeros.length - 1)];
        }
        for(let num of misNumeros) {
            ventana3.textContent = misNumeros[_.random(misNumeros.length - 1)];
        }
        if(ventana1.textContent == ventana2.textContent && ventana2.textContent == ventana3.textContent && ventana1.textContent == ventana3.textContent) {
            mensajeVictoria.classList.add('is-active');
        }
    }
    /// Cerrar mensaje de victoria
    function cerrarMensajeVictoria () {
        mensajeVictoria.classList.remove('is-active');
    }
    /// Cerrar mensaje de derrota
    function cerrarMensajeDerrota () {
        mensajeDerrota.classList.remove('is-active');
    }
    
    // Eventos
    /// Animacion palanca
    palanca.addEventListener('click', animarPalanca);
    insertCoin.addEventListener('click', sumarCreditos);
    botonCerrarMensajeVictoria.addEventListener('click', cerrarMensajeVictoria);
    recuadroVictoria.addEventListener('click', cerrarMensajeVictoria);
    botonCerrarMensajeDerrota.addEventListener('click', cerrarMensajeDerrota);
    recuadroDerrota.addEventListener('click', cerrarMensajeDerrota);
});